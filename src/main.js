// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import * as firebase from 'firebase'
import router from './router'
import { store } from './store'
import axios from 'axios'

// set baseUrl from app
axios.defaults.baseURL = 'http://jsonplaceholder.typicode.com'

Vue.use(Vuetify)
Vue.config.productionTip = false
Vue.prototype.$http = axios

axios.interceptors.response.use((response) => {
  return response
}, function (error) {
  if (error.response.status === 401) {
    console.log('unauthorized, logging out ...')
    // auth.logout()
    router.replace('/login')
  }
  return Promise.reject(error)
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyDMWJbTX4yp9e_ZSlwPnLjS-ZXtMyTFycQ',
      authDomain: 'abci-cd777.firebaseapp.com',
      databaseURL: 'https://abci-cd777.firebaseio.com',
      projectId: 'abci-cd777',
      storageBucket: 'abci-cd777.appspot.com'
    })
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
      }
    })
  }
})
