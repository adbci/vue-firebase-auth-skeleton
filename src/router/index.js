import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import About from '@/components/About'
import Profile from '@/components/User/Profile'
import Signup from '@/components/User/Signup'
import Signin from '@/components/User/Signin'
import Books from '@/components/books/Books'
import Products from '@/components/products/Products'
import BookEdit from '@/components/books/BookEdit'
import BookDetail from '@/components/books/BookDetail'
import Error404 from '@/components/errors/404'
import AuthGuard from './auth-guard'

/* lazy loading webpack */
/*
const BookStart = resolve => {
  require.ensure(['../components/books/404.vue'], () => {
    resolve(require('../components/books/BookStart.vue'))
  })
}
*/
Vue.use(Router)

const routes = [
  {
    path: '/404',
    name: '404',
    component: Error404
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    beforeEnter: AuthGuard
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup
  },
  {
    path: '/signin',
    name: 'Signin',
    component: Signin
  },
  {
    path: '/about',
    name: 'about',
    component: About,
    beforeEnter: (to, from, next) => {
      console.log('Inside route about')
      next()
    }
  },
  {
    path: '/users',
    component: Products
  },
  {
    path: '/books',
    name: 'Books',
    component: Books,
    beforeEnter: AuthGuard,
    children: [
      {
        path: ':id',
        name: 'bookDetail',
        component: BookDetail,
        beforeEnter: (to, from, next) => {
          console.log('Inside route detail')
          next()
        }
      },
      {
        path: ':id/edit',
        name: 'bookEdit',
        component: BookEdit,
        beforeEnter: (to, from, next) => {
          console.log('Inside route edit')
          next()
        }
      }
    ]
  },
  {
    path: '*', redirect: '/404'
  }
]

export default new Router({
  mode: 'history',
  routes,
  linkActiveClass: 'is-active'
})
