import axios from 'axios'

export default {
  state: {
    books: [],
    loadingBook: false,
    errorBook: []
  },
  getters: {
    loadingBook (state) {
      return state.loadingBook
    },
    errorBook (state) {
      return state.errorBook
    },
    books (state) {
      return state.books
    }
  },
  mutations: {
    setBooks (state, payload) {
      state.books = payload
    },
    setErrorBooks (state, payload) {
      state.errorBook = payload
    },
    setLoadingBook (state, payload) {
      state.loadingBook = payload
    },
    FETCH_USERS (state, users) {
      state.users = users
    }

  },
  actions: {
    getAllBooks ({commit}) {
      console.log('Get Books from books/store')
      this.state.error = this.state.books = null
      axios.get(`http://jsonplaceholder.typicode.com/posts`)
        .then(response => {
          // JSON responses are automatically parsed.
          commit('setBooks', response.data)
//          this.$state.books = response.data
          console.log(this.state.books)
        })
    }
    /*
    autoSignInTatz ({commit}, payload) {
      commit('setUser', {
        id: payload.uid,
        name: payload.displayName,
        email: payload.email,
        photoUrl: payload.photoURL
      })
    },
    logoutTatz ({commit}) {
      firebase.auth().signOut()
      commit('setUser', null)
    }
    */
  }
}
