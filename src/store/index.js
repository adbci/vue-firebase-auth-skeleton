import Vue from 'vue'
import Vuex from 'vuex'
import user from './user'
import book from './book'
import counter from './modules/numbers/counter'
import shared from './shared'
import * as actions from './actions'
import * as mutations from './mutations'
import * as getters from './getters'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export const store = new Vuex.Store({
  state: {
    value: 0
  },
  getters: getters,
  mutations: mutations,
  actions: actions,
  modules: {
    user: user,
    book: book,
    shared: shared,
    counter: counter
  },
  strict: debug
})
