import * as types from '../../types'
// import Api from '../../utils/api'
import axios from 'axios'
// import Vue from 'vue'

const state = {
  counter: 0,
  users: [],
  errors: [],
  user: null
}

const getters = {
  [types.DOUBLE_COUNTER]: state => {
    return state.counter
  },
  [types.CLICK_COUNTER]: state => {
    return state.counter + ' Clicks'
  },
  [types.GET_USERS]: state => {
    return state.users
  }
}

const mutations = {
  increment: (state, payload) => {
    state.counter += payload
  },
  decrement: (state, payload) => {
    state.counter -= payload
  },
  setUsers: (state, payload) => {
    state.users = payload
  },
  setUserById: (state, payload) => {
    state.user = payload
  }
}

const actions = {
  incrementAction: ({commit}) => {
    commit('increment')
  },
  decrementAction: ({commit}, payload) => {
    commit('decrement', payload)
  },
  asyncIncrementAction: ({commit}, payload) => {
    setTimeout(() => {
      commit('increment', payload.by)
    }, payload.duration)
  },
  asyncDecrementAction: ({commit}) => {
    setTimeout(() => {
      commit('decrement')
    }, 1500)
  },
  getUsers: ({commit}) => {
    axios.get(`http://jsonplaceholder.typicode.com/users`)
      .then(response => {
        commit('setUsers', response.data)
        console.log(response.data)
      })
      .catch(e => {
        this.errors.push(e)
      })
  },
  getUserById: ({commit}, payload) => {
    setTimeout(() => {
      commit('setUserById', payload.id)
    }, payload.duration)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
