import Axios from 'axios'

export default {
  get (url, request) {
    return Axios.get(url, request)
      .then((response) => Promise.resolve(response.body.data))
      .catch((error) => Promise.reject(error))
  },
  post (url, request) {
    return Axios.post(url, request)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  },
  patch (url, request) {
    return Axios.patch(url, request)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  },
  delete (url, request) {
    return Axios.delete(url, request)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  }
}
